# devops-netology

## Description terraform/.gitignore 
1. \*\*.terraform/* - игнорировать все файлы из вложенных каталогов .terraform
2. *.tfstate - игнорировать все файлы с расширением .tfstate
3. \*.tfstate.* - игнорировать все файлы имеющие в имени .tfstate.
4. crash.log - игнорировать все файлы crash.log
5. crash.*.log - игнорировать все файлы имеющие в имени crash.\*.log
6. *.tfvars - игнорировать все файлы с расширением *.tfvars
7. *.tfvars.json  - игнорировать все файлы имя которых оканчивается на .tfvars.json
8. override.tf - игнорировать файл override.tf
9. override.tf.json - игнорировать файл override.tf.json
10. *_override.tf - игнорировать все файлы имя которых оканчивается на _override.tf
11. *_override.tf.json - игнорировать все файлы имя которых оканчивается на _override.tf.json
12. .terraformrc - игнорировать файл .terraformrc
13. terraform.rc - игнорировать файл terraform.rc